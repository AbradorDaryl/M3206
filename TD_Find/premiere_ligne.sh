#!/bin/bash
#On affiche les premières lignes de tous les fichiers *.txt d'un répertoire 

echo "Entrez le répertoire contenant les fichiers .txt"
read repertoire

var=$(find / -name "$repertoire")

find $var -name *.txt -exec head -1 {} \;
