### Quelques exemples sur find

## Test de l'option -exec de find

Cela permet d'exécuter une commande sur le résultat de find.

Ci-dessous cela affiche tout les fichiers cacher ayant un nom avec une extension en .gif puis on exécute un ls sur la sortie du find. Autrement dit on liste tous les fichiers cacher.

```bash
find . -name *.gif -exec ls {} \;
```

## Test de l'option -ok de find

Avec la commande ci-dessous, on va rechercher dans le répertoire /tmp tous les fichiers ayant une extension .txt, puis on le supprime mais avec l'intervention de l'utilisateur pour confirmer suivant l'argument "y" ou "Y" dû à la présence du -ok .

On peut exécuter la commande suivant cette syntaxe :

```bash
find /tmp -name *.txt -ok rm {} \;
```

Si nous avons une erreur sur la commande on utilise la syntaxe suivante :

```bash
find /tmp -name "*.txt" -ok rm {} \;
```

## Combiner find et cp

La commande cpio permet de faire des sauvegardes.

On doit développer un script qui permet de copier tous les fichiers d'un répertoire soit :

```bash
echo "Entrez le répertoire à copier"
read repertoire

find / -name "$repertoire" -exec cp -r {} /tmp/"repertoire" \;
```
On doit maintenant trouver la commande qui permet de lister les premières lignes de tous les fichiers *.txt d'un répertoire soit :

Il faut au préalable créer des fichiers.txt avec des lignes de textes à l'intérieur.

```bash
echo "Entrez le répertoire contenant les fichiers .txt"
read repertoire

var=$(find / -name "$repertoire")
find $var -name *.txt -exec head -1 {} \;
```

### Quelques exemples de awk

On ecrit en awk un script qui n'écrit ni le premier ni le dernier champ de chaque ligne soit :

```bash
find /tmp -name *.txt -exec awk '{ print NF ":" $2 }' {} \;
```

On écrit maintenant un script qui ecrit le dernier champs de la dernière ligne d'un fichier avec awk (on utilise NF et END)


```bash
find /tmp -name *.txt -exec awk 'END{ print NF ":" $3}' {} \;
```

