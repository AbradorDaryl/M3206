#!/bin/bash
#script permettant de copier tous les fichiers d'un répertoire

echo "Entrez le repertoire à copier"
read repertoire

find / -name "$repertoire" -exec cp -r {} /tmp/"$repertoire" \;
