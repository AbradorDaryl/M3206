Ce dépot contiendra les exercices de TD et TP

Préalable : mkdir /M3206/td1


-----------------
|   Exercice 1	|
-----------------


1) Retour de la commande
Command : git status >> ../README.md

On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   ../README.md


2) Création fichier text
cd /M3206/td1
touch TD1_Git.txt
echo "Première ligne du fichier" >> TD1_Git.txt

3) Retour de la commande git status après l'édition du fichier TD1_Git.txt

command: git status >> ../README.md

On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   ../README.md

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   ../README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	./

4) ajout de modification dans l'index
git commit -m "publication de la modification"

5) retour de la command après avoir fait des modification dans l'index:

command : git status >> ../README.md


On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   ../README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	../.README.md.swp

no changes added to commit (use "git add" and/or "git commit -a")

6) envoi de la publication maintenant sur le dépot grâce à la commande:

git push origin master

7) retour de la commande git status après avoir fait un envoi sur le dépot distant

On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   ../README.md

no changes added to commit (use "git add" and/or "git commit -a")


--------------------------
|     EXERCICE 2         |
--------------------------

les log après l'ajout des plusieurs ligne
commit a525d2cc09567a7cf90ee76c4a2672c863ee1840
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 20:18:19 2016 +0400

    envoi de la quatrième ligne

commit eaa0a8428c1734f1031bd86487e12e76daec85e6
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 20:16:49 2016 +0400

    git add TD1_Git.txt

commit b03a4f1f5f13877d8024a93d2a48f9b310b90b4b
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 20:13:14 2016 +0400

    git add TD1_Git.txt

commit 39a62bf9ac9dcb517b9601870161c7b2d43b4dba
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 19:51:17 2016 +0400

    publication de la modification

commit bc8965969e036c327dd1c5101c5191e00d1124e5
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 17:00:44 2016 +0400

    add README>

commit 30bd0ce0d295a4adc3e6ef300b6dd08ba8694d47
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 16:17:29 2016 +0400

    add README

--------------------
|   EXERCICE 3     |
--------------------

1) Git status après l'ajout de la 5eme ligne
echo "cinquième ligne" >> TD1_Git.txt

On branch master
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   ../README.md
	modified:   TD1_Git.txt

2) Après être revenue à la version précédente:

git checkout --  TD1_Git.txt

no changes added to commit (use "git add" and/or "git commit -a")
On branch master
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   ../README.md

no changes added to commit (use "git add" and/or "git commit -a")



-------------------------
|     EXERCICE 4	|
-------------------------

1) Ajout a nouveau de la 5eme ligne
echo "5eme ligne" >> TD1_Git.txt

Ajout de la modification dans l'index
git add TD1_Git.txt


Visualisation du git status:

On branch master
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   TD1_Git.txt

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   ../README.md


3) revenir à la version précédente de l'index:
git reset HEAD TD1_Git.txt

enlever le fichier de l'index:
git checkout -- TD1_Git.txt

Vérification des logs via git status:


On branch master
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   ../README.md

no changes added to commit (use "git add" and/or "git commit -a")


---------------
|  EXERCICE 5 |
---------------

git revert HEAD~1..HEAD permet de revenir à la version 3 lignes, autrement dit on crée un commit qui enlève le dernier commit


-------------
| EXERCICE 6|
-------------

On envoi maintenant les modifications sur le dépot distant :
> git push


-------------
| EXERCICE 7|
-------------

Création d'une branche TEST
> git branch TEST

Positionnement dans cette même branche
> git checkout TEST

Modification du fichier TD1_Git.txt
> cd /M3206/td1
> echo "LIGNE: Ajouté depuis la branche TEST" >> TD1_Git.txt
> git add TD1_Git.txt
> git commit -m "modification depuis branch"
> git checkout master (revenir à la branche principale)

Une fois revenue dans la branche master on peut voir que la ligne ajouter depuis branch n'apparait pas  et qu'il y a uniquement trois ligne.

On incluons maintenant les modifications faite à partir de branch à notre branche master
> git merge TEST

une fois la commande précédente faite, le fichier TD1_Git.txt contient la ligne de texte que l'on a ajouter depuis branch


voici les log (git log):

commit 45cfa7a362069fcf7385391591e510c05d863921
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 21:47:52 2016 +0400

    modification depuis branch

commit 2811c2ab9e0dfb1a154c4c287799c602142b440c
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 20:40:51 2016 +0400

    Revert "envoi de la quatrième ligne"
    
    This reverts commit a525d2cc09567a7cf90ee76c4a2672c863ee1840.

commit a525d2cc09567a7cf90ee76c4a2672c863ee1840
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 20:18:19 2016 +0400

    envoi de la quatrième ligne

commit eaa0a8428c1734f1031bd86487e12e76daec85e6
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 20:16:49 2016 +0400

    git add TD1_Git.txt

commit b03a4f1f5f13877d8024a93d2a48f9b310b90b4b
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 20:13:14 2016 +0400

    git add TD1_Git.txt

commit 39a62bf9ac9dcb517b9601870161c7b2d43b4dba
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 19:51:17 2016 +0400

    publication de la modification

commit bc8965969e036c327dd1c5101c5191e00d1124e5
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 17:00:44 2016 +0400

    add README>

commit 30bd0ce0d295a4adc3e6ef300b6dd08ba8694d47
Author: Abrador <d.abrador@rt-iut.re>
Date:   Tue Nov 15 16:17:29 2016 +0400

    add README
