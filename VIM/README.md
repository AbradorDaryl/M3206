### Exercice 1

Pour ajouter la ligne : *temp var7 11

```bash
 6j ( descendre de 6 lignes )	
 i ( insertion du texte )
 *temp var7 11
 on ajoute un saut à la ligne
```

On ajoute "New text" tout en sachant que le curseur le curseur est au niveau de la ligne de *temp var7 11 :

```bash
 4j ( on dscend de 4 lignes )
 i ( insertion )
 (saut de ligne)
 ajout de New text.
 (saut de ligne)
```


### Exercice 2

On remplace Sed par XXX

```bash
 :%s/Sed/XXX/g
```

Le %s permet de prendre tout le texte en compte
Le /Sed/XXX/g permet de remplacer tout les Sed par XXX de manière globale

On remplace sed par xxx

```bash
 :%s/sed/xxx/g
```

### Exercice 3

On commence par supprimer le blog inutile

```bash
 j
 3dd
```

On remplace ce qu'il y a entre '( )'

```bash
 /a
 ci(
 *a
```

On modifie ce qu'il y a après Hello

```bash
 j
 /H
 ci"
 Hello #{a.join(',')}
```

Exercice 4

```
> 99
> r
> i
> i
> i
```
 
