#!/bin/bash
frst="first"
lst="last"

#exit 1 permet d'indiquer que l'on sait qu'il y a une sortie d'erreur et donc d'arrêter le script sans montrer l'erreur 

#On vérifie si l'argument entrer est différent de first ou last
if ([ $1 != $frst ] && [ $1 != $lst ]); then
echo "First argument should be first or last."
echo "usage ./datecomm.sh [first|last] command file"
exit 1
fi

#On vérifie que l'argument pour le fichier existe
if [ ! -e $3 ]; then
echo "Le fichier "$3" n'existe pas."
echo "usage: ./datecomm.sh [first|last] command file"
exit 1 
fi


#On teste si la variable est inexistante ou vide
if ([ -z $1 ] || [ -z $2 ] || [ -z $3 ]); then
echo "The command takes 3 arguments."
echo "usage: ./datecomm.sh [first|last] command file"
exit 1
fi


#On vérifie que l'historique contient la commande rechercher en comptant le nombre de fois qu'il apparait
apparition=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ';' -f2 | wc -l)
if [ $apparition -eq 0 ]; then
echo "The history does not contain "$2" command"
exit 1
fi


#Si l'argument entrer est first
if [ $1 = $frst ]; then
time=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | sort -r | head -1)
fi

#Si l'argument entrer est last
if [ $1 = $lst ]; then
time=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | sort -r | head -1)
fi

#On affiche le timestamp compréhensible par l'homme
date -d @$time
