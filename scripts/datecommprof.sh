#!/bin/bash
#Initialisation de deux variables

frst="first"
lst="last"

#On vérifie si l'utilisateur à entrer comme paramètre "first"
if [ $1 = $frst ]; then
time=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | head -1)
fi

#On vérifie si l'utilisateur à entrer comme paramètre "last"

#ici sort -r permet d'inverser la sortie afin d'afficher ce qui était en dernier, en premier
#head -1 permet d'afficher la première commande référencé

if [ $1 = $lst ]; then
time=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | sort -r | head -1)
fi

#On affiche le timestamp compréhensible par l'homme
date -d @$time

#execution
#cd /root/M3206/scripts
#bash datecommprof.sh <first_ou_last> <command> <chemin_absolue/relative>/my_history
