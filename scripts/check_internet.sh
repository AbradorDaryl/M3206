#!/bin/bash
# On lance une requête ping pour vérifier la connexion tout en faisant en sorte que cette requête soit détruite une fois le test fait sans afficher à l'écran avec donc une redirection de sortie

echo "[...] Checking internet connection [...]"

ping -c 5 8.8.8.8 >/dev/null 2>&1

#condition de verification if pour savoir si la sortie donne un booléan 0 ça fonctionne sinon ça fonctionne pas, donc pas d'internet

if [ $? -eq 0 ]; then
	echo "[...] Internet access OK [...]"
else
	echo "[/!\] Not connected to Internet [/!\]"
	echo "[/!\] Please check configuration [/!\]"
fi

#En sortie si le ping fonctionne nous avons le premier message dans le cas contraire nous avons les deux messages d'erreurs    
