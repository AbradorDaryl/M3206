#!/bin/bash

#Affichage de la commande donnée en paramètre
#awk '{print $2}' permet d'afficher uniquement la deuxième colonne
#grep $1 permet de garder uniquement les lignes qui contiennent la commande donné en paramètre
#cut -d ':' -f1 permet de limiter la chaine jusqu'à ':' et de prendre la partie avant ses ':'
#head -1 permet d'avoir la première commande référencé taper

timescomm=$(cat $2 | awk '{print $2}' | grep $1 | cut -d ':' -f1 | head -1)


#On affiche la date où la commande à été entrer
# notamment ici compréhensible par l'homme grâce à l'option -d @
date -d @$timescomm

# Pour le fonctionnement on fait 
# cd /root/M3206/scripts
#bash datecommprof.sh <commande> <chemin_absolue>/my_history
