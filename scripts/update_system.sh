#!/bin/bash
# Création d'une condition pour faire update / upgrade du système en vérifiant si l'utilisateur est en root ou non

# si l'UID est 0 (qui est celui de root) alors nous ferons les commandes update et upgrade qui est redirigé pour ne pas avoir d'affichage sur l'ecran
 
if [ "$UID" -eq "0" ]; then
	echo "[...] update database [...]"
	apt-get update >/dev/null 2>&1
	echo "[...] upgrade system [...]"
	apt-get upgrade >/dev/null 2>&1
else
	echo "[/!\] Vous devez être super-utilisateur [/!\]"
fi

