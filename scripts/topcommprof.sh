#!/bin/bash

#On veut le paramètre pour savoir le nombre de commandes à afficher

#recupère l'emplacement du fichier my_history
var=$(find / -name "my_history")

#On affiche le top des commandes 

cat $var | awk '{print $2}' | cut -d ';' -f2 | sort | uniq -c | sort -nr | head -$top 

# cat $var | awk '{print $2}' permet de lister en prenant que la colonne de droite
# cut -d ';' -f2 permet d'enlever ce qu'il y a avant le ';' et permet de prendre en compte le deuxième champ
# sort permet de trier 
# uniq -c permet d'enlever les doublons
# sort -nr permet de mettre des numéro sur le trie
# head permet d'avoir les plus utilisés
