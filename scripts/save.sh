#!/bin/bash
#On fait un script de sauvegarde pour le contenu d'un répertoire donnée en paramètre

#Permet de demander la saisie
echo "Entrez le repertoire en paramètre"
read repertoire

#la variable var conttient le chemin absolue vers le répertoire entrer en paramètre

var=$(find / -name "$repertoire")

#vérification que /tmp/backup exite
if [ -d /tmp/backup ]; then
echo "Le répertoire backup existe"
else
mkdir /tmp/backup >>/dev/null 2>&1
fi

#On crée l'archive.tar.gz en précisant entre guillements les chemins des dossiers au sauvegarder
# $basename reprend uniquement la chaine de caractère entrer pour le répertoire que l'on veut rendre en tant qu'archive 
# $date permet d'avoir : année, mois, jours, heure, minutes

echo "création de l'archive"

touch $var/chemin_absolue.txt
echo $var>>$var/chemin_absolue.txt
tar -cvzf /tmp/backup/$(date +%Y_%m_%d_%H%M)_$(basename $repertoire) "$var" >>/dev/null 2>&1

echo "Votre archive a été créer"
