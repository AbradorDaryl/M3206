#!/bin/bash
#On va vérifier si un outil est installé en utilisant la commande dpkg avec l'option -l pour lister les paquets qui correspond aux outils

#on vérifie la présence des paquets correspondant à git

dpkg -l git >>/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "[...] git: installé [...]"
else
	echo "[/!\] git: pas installé [/!\] lancer la commande 'apt-get install git'"
fi

#on vérifie la présence des paquets correspondant à tmux

dpkg -l tmux >>/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "[...] tmux: installé [...]"
else
	echo "[/!\] tmux: pas installé [/!\] lancé la commande 'apt-get install tmux'"
fi

#on vérifie la présence des paquets correspondant à vim

dpkg -l vim >>/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "[...] vim: installé [...]"
else
	echo "[/!\] vim: pas installé [/!\] lancé la commande 'apt-get install vim'"
fi

#on vérifie la présence des paquets correspondant à htop

dpkg -l htop >>/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "[...] htop installé [...]"
else
	echo "[/!\] htop: pas installé [/!\] lancé la commande 'apt-get install htop'"
fi
