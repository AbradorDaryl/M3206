#On collecte les informations d'historique de commandes
HISTFILE=~/.bash_history 

#On active l'utilisation de la commande history dans le script
set -o history

echo "top 10 commandes utilisés"

history | awk '{print $2}' | sort | uniq -c | sort -nr | head -10

# history | awk '{print $2}' permet de lister en prenant que la colonne de droite
# sort permet de trier 
# uniq -c permet d'enlever les doublons
# sort -nr permet de mettre des numéro sur le trie
# head -10 permet d'avoir les 10 les plus utilisés

echo "top 5 commandes utilisés"
history | awk '{print $2}' | sort | uniq -c | sort -nr | head -5

