echo "[...] Lancement de tout les scripts [...]"

#On lance tout les scripts grâce à la commande: bash <chemin_acces_script>

bash /root/M3206/scripts/check_internet.sh
bash /root/M3206/scripts/check_ssh.sh
bash /root/M3206/scripts/check_tools.sh
bash /root/M3206/scripts/update_system.sh

echo "[...] Fin du lancement des scripts [...]"
