#!/bin/bash
#On vérifie que le démon ssh est bien installer et qu'il tourne

#on vérifie que ssh est installer 

dpkg -l ssh >>/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "[...] ssh: est installé [...]"
else
	echo "[/!\] ssh: le service n'est pas lancé  [/!\]"
fi

#on vérifie que le démon ssh tourne
ps aux | grep [s]shd >>/dev/null 2>&1

#on met [s]shd pour effectuer la vérification uniquement pour le démon ssh sans prendre en compte le processus de grep ssh

if [ $? -eq 0 ]; then
	echo "[...] ssh: le service est lancé [...]"
else
	echo "[...] ssh: le service n'est pas lancé [...]"
	echo "[...] ssh: lancement du service [...]"
/etc/init.d/ssh start
fi
