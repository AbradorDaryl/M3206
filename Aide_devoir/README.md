## Aide mémoire de manipulation fichiers

### Exemples sur le find

#### Recherches simples
Recherche sur des fichiers suivant le type d'extension + son poid

```bash
find / -name "*.txt" -exec ls -lh {} \;
```

Nous avons de même cette syntaxe que l'on utilisera uniquement dans ce cas pour aller plus vite

```bash
find / -name "*.txt" | xargs ls -lh
```

Recherche sur pour savoir combien de fichier 

```bash
find / -name "*.txt" -exec ls -l {} | wc -l
```

A noter qu'après -exec on peut faire des combinaisons mais il ne faut pas oublié de mettre " {} \;" après la première commande pour que la sortie soit faite sur tous les fichiers que find trouve.
