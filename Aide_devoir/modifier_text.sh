#!/bin/bash
#On recherche une chaine de caractère puis on le modifie
echo "Exemple en utilisant un find en premier"
find / -name "test.txt" | xargs grep -rl "bonjour" | xargs sed -i "s/bonjour/non jamais/g"

echo "modification de mots stop"
find / -name "test.txt" | xargs grep -rl "stop"  | xargs sed -i "s/stop/bon devoir/g"

