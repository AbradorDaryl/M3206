#!/bin/bash
#On fait une recherche suivant l'extension fichier puis on le liste
#extension fichier : *.gif, *.txt, *.docx, *.png etc
#xargs est une alternatif à -exec mais nous 
#on affiche de même le poid du fichier
#Il faut tester en enlevant les commentaires

#echo "exemple générale suivant xargs"
#find / -name "*.txt" | xargs ls -lh 

#echo "exemple suivant -exec"
#find / -name "*.txt" -exec ls -lh {} \; 

echo "Recherche du fichier titi.txt et toto.txt"
find / -name "toto.txt" -exec ls -lh {} \;
find / -name "titi.txt" -exec ls -lh {} \;
